"""
Simulate mutations in a genomic region given a mutational profile
"""

import bgreference
import bgsignature
import numpy as np


def mut_probabilities(chromosome, seq_start, seq_end, genome, path_to_mutprofile, kmer_size):
    """
    Generate mutational probabilities per position of a given a human genomic region using a mutational profile.

    The mutational profile is a dictionary with reference kmer>nucleotide as keys and frequencies for a whole cohort
    as values. This can be obtained using bgsignature.

    This function can only analyze sequences that do not contain missing reference nucleotides ('N').

    The algorithm returns the array of mutational probabilities for each reference nucleotide (start and end included)
    to any of the three possible alternates (following A, C, G, T).

    Args:
        chromosome (str): chromosome (ex. '1', 'X')
        seq_start (int): start position of the sequence
        seq_end (int): end position of the sequence
        genome (str): human reference genome ('hg19' or 'hg38')
        path_to_mutprofile (str): path to mutational profile file (json format)
        kmer_size (int): kmer size of the mutational profile

    Returns:
        probabilities (list): list of mutational probabilities. The length of the output array is three times
        the reference input sequence.

    """

    probabilities = list()
    nucleotides = {'A', 'C', 'G', 'T'}
    # include kmer sequence for the first and last nucleotides [start - delta, end + delta]
    delta = kmer_size // 2

    # Check mutational profile
    try:
        signatures_d = bgsignature.file.load(file=path_to_mutprofile)
    except UnicodeDecodeError:
        raise Exception('Error in input signatures file {}'.format(path_to_mutprofile))

    # Check sequence
    ref_sequence = ''
    unknown_bp = 0
    if seq_start < seq_end:
        size = seq_end - seq_start + 1 + delta * 2   # length and margins included
        ref_sequence = bgreference.refseq(genome, chromosome, seq_start - delta, size)
        unknown_bp = ref_sequence.count('N')
    else:
        raise Exception('Error. Sequence start > end')

    if unknown_bp == 0:
        for n in range(delta, len(ref_sequence) - delta):  # start to end +
            ref_kmer = ref_sequence[n - delta: n + delta + 1]
            # calculate mutational prob to any other kmer
            # sort alternates to keep track
            for alt in sorted(list(nucleotides.difference({ref_kmer[delta]}))):
                probabilities.append(signatures_d.get('{}>{}'.format(ref_kmer, alt), 0))
    else:
        raise Exception('Error. Sequence cannot be analyzed. There are unknown nucleotides in the sequence provided.')

    return probabilities


def normalize(probabilities):
    """
    Given an array of probabilities, normalize them to 1

    Args:
        probabilities (list): list of probabilities

    Returns:
        normalized_probabilities (list): normalized probabilities

    """

    if sum(probabilities) != 0:
        prob_factor = 1 / sum(probabilities)
        normalized_probabilities = [prob_factor * p for p in probabilities]
    else:
        normalized_probabilities = probabilities

    return normalized_probabilities


def simulate_mutations(
        seq_start,
        seq_end,
        mutations,
        probabilities,
        kmer_size,
        n_simulations,
        window_simulation,
        seed
    ):

    """
    Simulates mutations inside a sequence.

    Mutations will only be simulated inside the region. This means that regions that partially fall outside
    the region will be displaced to fit inside. If simulating windows are longer than the region, the simulation
    windows will be trimmed to fit inside.

    Only those mutations that fall inside the region are simulated.

    Args:
        seq_start (int): start position of the sequence
        seq_end (int): end position of the sequence
        probabilities (list): list of mutational probabilities; 3 alternates per position
        kmer_size (int): kmer size of the mutational profile
        n_simulations (int): number of simulations
        window_simulation (int): window size where mutations are simulated
        seed (int): randomization seed

    Returns:
        df_simulated_mutations (list): list of lists with simulated mutation positions. List of length equal to the
        total number of mutations simulated. Each sublist contains n simulations per mutation.
    """

    delta = kmer_size // 2
    half_window = window_simulation // 2
    np.random.seed(seed)
    df_simulated_mutations = []

    # Check mutations are inside the region of analysis, otherwise skip
    mutations = [i for i in mutations if i in range(seq_start, seq_end + 1)]

    if seq_start < seq_end:
        if probabilities:
            # Simulate mutations
            for mutation in mutations:
                # First, get coordinates of randomization window
                expected_window_begin = mutation - half_window
                expected_window_end = mutation + half_window

                """Check that randomization window does not expand the analyzed sequence chunk, if so, 
                displace it or trim it"""
                if (seq_end - seq_start + 1) >= window_simulation:
                    check_5 = expected_window_begin < seq_start
                    check_3 = expected_window_end > seq_end

                    if check_5:
                        window_begin = seq_start
                        window_end = seq_start + window_simulation - 1  # window //2 per side

                    elif check_3:
                        window_end = seq_end
                        window_begin = seq_end - window_simulation + 1  # window //2 per side

                    else:
                        window_begin = expected_window_begin
                        window_end = expected_window_end
                else:
                    window_begin = seq_start
                    window_end = seq_end

                # Then map mutations to index in sequence
                # 3* accounts for alternates in the array of probabilities
                start_index = 3 * (window_begin - seq_start)
                end_index = 3 * (window_end - seq_start)
                # end +1, range and slice
                simulations = np.random.choice(range(start_index, end_index + 1), size=n_simulations,
                                                   p=normalize(probabilities[start_index:end_index+1]))
                # Convert index to mutation position
                list_simulations_per_mutation = []
                for count, index in enumerate(simulations):
                    list_simulations_per_mutation.append(seq_start + index // 3)
                df_simulated_mutations.append(list_simulations_per_mutation)
        else:
            raise Exception('Error. No probabilities provided')
    else:
        raise Exception('Error. Sequence start > end')

    return df_simulated_mutations


if __name__ == '__main__':
    chromosome = '17'
    pos_start = 7673307
    pos_end = 7673339
    genome = 'hg38'
    path_to_mutprofile = './TCGA_WXS_PAAD_profile.json'
    kmer_size = 3
    mutations = [7673308, 7673310, 7673310, 7673310, 7673315, 7673318]
    n_simulations = 10
    window_simulation = 31
    seed = 632376

    # Get the array of mutational probabilities for the region of interest
    probabilities = mut_probabilities(chromosome, pos_start, pos_end, genome, path_to_mutprofile, kmer_size)
    # Simulate mutations
    simulated_mutations = simulate_mutations(
        pos_start,
        pos_end,
        mutations,
        probabilities,
        kmer_size,
        n_simulations,
        window_simulation,
        seed
    )
    print(simulated_mutations)
