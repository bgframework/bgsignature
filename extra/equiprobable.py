"""
Create a file with equiprobable k-mers (all to 1)
"""
import itertools
import json
import sys


def equiprobable(kmer, includeN=False):
    seq = 'ACGTN' if includeN else 'ACGT'
    counts = {}
    for triplet in itertools.product(seq, repeat=kmer):
        for alt in 'ACGT':
            if alt == triplet[kmer//2] or 'N' == triplet[kmer//2]:
                continue
            counts[''.join(triplet) + '>' + alt] = 1
    return counts


def write(counts):
    print(json.dumps(counts, indent=4))


if __name__ == '__main__':
    kmer = int(sys.argv[1])
    write(equiprobable(kmer))
